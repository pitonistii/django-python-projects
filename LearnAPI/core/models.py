from django.db import models

# Create your models here.
class Person(models.Model):
    name =  models.CharField(max_length=200)
    age = models.IntegerField(default=1)
    job = models.CharField(max_length=200, default="somer")
    created = models.DateTimeField(auto_now_add=True)
